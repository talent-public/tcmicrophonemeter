# Changelog

<a name="0.0.1"></a>
## 0.0.1 (2020-10-06)

### Added

- ✨ Update example project [[536ac9b](https://gitlab.com/talent-public/tcmicrophonemeter/commit/536ac9b8f398123426f4aa01ae1b1b396f27ce16)]
- ✨ Update example project [[7f9a601](https://gitlab.com/talent-public/tcmicrophonemeter/commit/7f9a6010aa2199671987dd22e8d568d1286e234e)]
- 👷‍♂️ Add podspec [[06d7545](https://gitlab.com/talent-public/tcmicrophonemeter/commit/06d75452ac1e3476ae8725eee77c3baff0ae7af3)]
- ✨ Add lib file [[580a3b0](https://gitlab.com/talent-public/tcmicrophonemeter/commit/580a3b0c9157110d7ea8c5747958ae90ff4e54a4)]
- ✨ Initial project setup [[d99e100](https://gitlab.com/talent-public/tcmicrophonemeter/commit/d99e1000fd6bf0ae34d7b3bd1ba16ae00b9c65f0)]

### Changed

- 🍱 Add image assets [[16aad49](https://gitlab.com/talent-public/tcmicrophonemeter/commit/16aad499f860b43bddaf87e86d9a89f087933d39)]

### Miscellaneous

- 📝 Add readme [[bae2e84](https://gitlab.com/talent-public/tcmicrophonemeter/commit/bae2e84478284c9a6945fc1181f029dc23f63b38)]
- 📄 Add license [[f8a3b1f](https://gitlab.com/talent-public/tcmicrophonemeter/commit/f8a3b1f7f2c7c339e9bf7b21c144034eddcb9ff5)]
-  Initial Commit [[5283f77](https://gitlab.com/talent-public/tcmicrophonemeter/commit/5283f771aaef6e9980a4e58d9932eca36bc5b393)]



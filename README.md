# TCMicrophoneMeter for iOS

This library helps you visualize user voice from input microphone

# How to use

## Installation

1. Add this line in your `Podfile`

```
pod 'TCMicrophoneMeter', :git => 'https://gitlab.com/talent-public/tcmicrophonemeter.git', :branch => 'main'
```

2. Run this command

```
pod install
```

## Sample code

```Swift
import UIKit
import TCMicrophoneMeter

class ViewController: UIViewController {

    lazy var meterView: MicrophoneMeterView = {
      let view = MicrophoneMeterView()
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.meterView)

        meterView.translatesAutoresizingMaskIntoConstraints = false
        meterView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        meterView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        meterView.widthAnchor.constraint(equalToConstant: 164).isActive = true
        meterView.heightAnchor.constraint(equalToConstant: 164).isActive = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        meterView.willAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        meterView.willDisappear()
    }

}
```

//
//  MicrophoneMeterView.swift
//  MicrophoneMonitor
//
//  Created by Peerasak Unsakon on 5/10/2563 BE.
//

import Foundation
import UIKit

open class MicrophoneMeterView: UIView {
    
    public var microphoneMonitor: MicrophoneMonitor?
    
    @IBInspectable public var microphoneImage: UIImage? {
        didSet { self.microphoneImageView.image = self.microphoneImage }
    }
    
    private lazy var microphoneImageView: UIImageView = {
       var imageView = UIImageView()
        imageView.image = self.microphoneImage
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private lazy var barView: UIView = {
       var view = UIView()
        view.backgroundColor = UIColor(red: 0.00, green: 0.70, blue: 0.27, alpha: 1.00)
        return view
    }()
    

    private var heightConstraint: NSLayoutConstraint?
    
    private var isActive: Bool = false
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.configure()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.configure()
    }
    
    private func configure() {
        
        self.addObservers()
        
        clipsToBounds = false
        
        self.addSubview(self.microphoneImageView)
        self.addSubview(self.barView)
        
        self.microphoneMonitor = MicrophoneMonitor()
        self.microphoneMonitor?.delegate = self
        
        //microphone icon
        self.microphoneImageView.translatesAutoresizingMaskIntoConstraints = false
        self.microphoneImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.microphoneImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.microphoneImageView.widthAnchor.constraint(equalToConstant: 18).isActive = true
        self.microphoneImageView.heightAnchor.constraint(equalToConstant: 31).isActive = true
        
        //meter bar
        self.barView.translatesAutoresizingMaskIntoConstraints = false
        self.barView.widthAnchor.constraint(equalToConstant: 7).isActive = true
        self.barView.centerXAnchor.constraint(equalTo: self.microphoneImageView.centerXAnchor).isActive = true
        self.barView.bottomAnchor.constraint(equalTo: self.microphoneImageView.topAnchor, constant: 20).isActive = true
        
        self.barView.layer.cornerRadius = 3
        self.barView.layer.masksToBounds = true
        
        self.heightConstraint = self.barView.heightAnchor.constraint(equalToConstant: 18)
        self.heightConstraint?.isActive = true
    }
    
    private func normalizeSoundLevel(level: Float) -> CGFloat {
        let maximumHeight: CGFloat = 18
        let level = max(0.2, CGFloat(level) + 50) / 2 // between 0.1 and 25
        let height = CGFloat(level * (maximumHeight / 25)) // scaled to max at 300 (our height of our bar)
        
        return height
    }
    
    private func addObservers() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    private func removeObservers() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    public func startMonitoring() {
        guard let microphoneMonitor = self.microphoneMonitor else {
            return
        }
        microphoneMonitor.startMonitoring()
    }
    
    public func stopMonitoring() {
        guard let microphoneMonitor = self.microphoneMonitor else {
            return
        }
        microphoneMonitor.stopMonitoring()
    }
    
    public func willAppear() {
        self.isActive = true
        self.startMonitoring()
    }
    
    public func willDisappear() {
        self.isActive = false
        self.stopMonitoring()
    }
    
    @objc private func didBecomeActive() {
        guard self.isActive else { return }
        self.startMonitoring()
    }
    
    @objc private func willResignActive() {
        guard self.isActive else { return }
        self.stopMonitoring()
    }
    
    deinit {
        self.removeObservers()
    }
    
}

extension MicrophoneMeterView: MicrophoneMonitorDelegate {
    func microphoneAccessDenied() {
        
    }
    
    func microphoneError(error: Error) {
        
    }
    
    func sampleChanged(currentLevel: Float) {
        guard let heightConstraint = self.heightConstraint else { return }
        let height = normalizeSoundLevel(level: currentLevel)
        heightConstraint.constant = height
        heightConstraint.isActive = true
        self.layoutIfNeeded()
    }
}


//
//  MicrophoneMonitor.swift
//  MicrophoneMonitor
//
//  Created by Peerasak Unsakon on 5/10/2563 BE.
//

import Foundation

import AVFoundation

@objc protocol MicrophoneMonitorDelegate: NSObjectProtocol {
    func sampleChanged(currentLevel: Float)
    func microphoneAccessDenied()
    func microphoneError(error: Error)
}

open class MicrophoneMonitor {
    
    private var audioRecorder: AVAudioRecorder
    private var timer: Timer?
    
    weak var delegate: MicrophoneMonitorDelegate?
    
    init() {
        
        let audioSession = AVAudioSession.sharedInstance()
        if audioSession.recordPermission != .granted {
            audioSession.requestRecordPermission { _ in }
        }
        
        let url = URL(fileURLWithPath: "/dev/null", isDirectory: true)
        let recorderSettings: [String:Any] = [
            AVFormatIDKey: NSNumber(value: kAudioFormatAppleLossless),
            AVSampleRateKey: 44100.0,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue
        ]
        
        do {
            self.audioRecorder = try AVAudioRecorder(url: url, settings: recorderSettings)
            try audioSession.setCategory(.playAndRecord, mode: .default, options: [])
            self.startMonitoring()
        } catch {
            fatalError(error.localizedDescription)
        }
    }

    public func startMonitoring() {
        self.audioRecorder.isMeteringEnabled = true
        self.audioRecorder.record()
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: { (timer) in
            self.audioRecorder.updateMeters()
            let currentLevel = self.audioRecorder.averagePower(forChannel: 0)
            self.delegate?.sampleChanged(currentLevel: currentLevel)
        })
    }
    
    public func stopMonitoring() {
        self.timer?.invalidate()
        self.audioRecorder.stop()
    }
    
    deinit {
        self.stopMonitoring()
    }
    
}

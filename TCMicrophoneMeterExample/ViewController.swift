//
//  ViewController.swift
//  TCMicrophoneMeterExample
//
//  Created by Peerasak Unsakon on 6/10/2563 BE.
//

import UIKit

class ViewController: UIViewController {

    var meterView: MicrophoneMeterView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        self.meterView = MicrophoneMeterView()

        guard let meterView = self.meterView else {
            return
        }

        meterView.microphoneImage = UIImage(named: "icon_microphone")

        self.view.addSubview(meterView)
        self.view.backgroundColor = .white
        meterView.backgroundColor = .clear

        meterView.translatesAutoresizingMaskIntoConstraints = false
        meterView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        meterView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        meterView.widthAnchor.constraint(equalToConstant: 164).isActive = true
        meterView.heightAnchor.constraint(equalToConstant: 164).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let meterView = self.meterView else { return }
        meterView.startMonitoring()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let meterView = self.meterView else { return }
        meterView.stopMonitoring()
    }
    
    deinit {
        print("viewcontrolller deinit")
    }

}
